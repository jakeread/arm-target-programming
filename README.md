# Better Documentation Elsewhere

This has been largely superceded by [the practical microcontroller primer](https://mtm.cba.mit.edu/2021/2021-10_microcontroller-primer/) over on MTM. Please use that first before bothering with this relic! 

# Using CMSIS-DAP 

[CMSIS-DAP](https://www.keil.com/support/man/docs/dapdebug/dapdebug_introduction.htm) is an *interface firmware* that connects microcontrollers to our PCs over USB, specifically for debugging code running on our microcontrollers, or loading new code onto them. 

If this is already confusing, you can check [this out](prog-doc.md).

## 1: Connect CMSIS-DAP to your Device 

CMSIS-DAP tools will normally have the standard JTAG Pinout, which is 2x5, this is true of the MAXDAP that we have in the lab: 

![dap](max32625pico_maxdap.png)

To connect this to your circuit, you'll want to put a 2x5 header on your circuit:
- connect pins 3, 5, and 9 to GND
- connect 10 to the microcontroller's reset line
    - you might also want to pull up the reset line
    - you might also want a button to reset the board manually 
    - some boards need an RC filter on the reset line, beware 
- connect 1 to the microcontroller's voltage 
- *if your chip uses SWD (most SAMD chips) 
    - connect SWDIO to SWDIO (read the datasheet!)
    - connect SWDCLK to SWDCLK (consider a pullup on this line as well)

Hopefully you already have a circuit with a working debug port (maybe one that someone else designed), if you're starting from scratch, a strategy I often use to make sure I will have programming success is to copy someone else's basic schematic: i.e. the [Adafruit Trinket M0](https://github.com/adafruit/Adafruit-Trinket-M0-PCB) is a good ground-zero for the SAMD21E18 chip, and the [Adafruit Feather M4](https://github.com/adafruit/Adafruit-Feather-M4-Express-PCB) is a good ground-zero for the SAMD51J19. Thanks adafruit! One note: they don't use the 2x5 pin header, instead, they use pogo pins on the board to break out SWDIO and SWDCLK. Just swap in a connector!

**Note** the CMSIS-DAP tool does not *provide* power to your circuit, it merely reads it to check that the device is alive. When you're programming the circuit, be sure to power it as well! For this reason, I often put a USB connector on my circuits to easily supply 5v. 

### SAMD51 Example

I built a barebones example for this chip, showing how I connect the 2x5-pin programming header, as well as set up the basic components I'll need (power regulation, USB connection) to get it running. Take note that some of the connections (SWD.. USB..) are not drawn completely: the nets are [named in eagle to connect them](https://electronics.stackexchange.com/questions/104575/how-to-create-wireless-networks-in-eagle-cad). 

This schematic will work with the Feather M4 Bootloader. Note that the KHZ Crystal *is necessary* for the bootloader to work, but is not necessary for you to program your own system. 

![samd51](samd51-example/schematic.png)

### SAMD21 Example 

Here's another, for the SAMD21. This schematic will work with the Trinket M0 Bootloader. 

![samd21](samd21-example/schematic.png)

## 2: Connecting CMSIS-DAP to your Computer 

A CMSIS-DAP tool itself should announce itself to your computer automatically (over usb). I am using the [MAXDAP](https://www.maximintegrated.com/en/products/microcontrollers/MAX32625PICO.html) on a windows machine. When I plugged this in and it appears in my device manager as 'CMSIS-DAP', and drivers were automatically installed, so I think I'm clear to proceed. 

Now I need to get a software system that will talk to the CMSIS-DAP tool. 

## 3: Flashing .hex or .bin via CMSIS-DAP and pyOCD

Sometimes, the move is to use a tool like CMSIS-DAP to load a bootloader onto our device, and then just use that bootloader routinely to re-program it / develop code.

To do this, you'll want to borrow a bootloader from someone who's written lots of them: I use [Adafruit Bootloaders](https://github.com/adafruit/ArduinoCore-samd/tree/master/bootloaders/) fairly often. To ensure that this works, I need to make that my circuit is compatible with the bootloader I'm going to use. The **Trinket M0 Bootloader** is a great, lightweight bootloader for the **SAMD21E18** and the **Feather M4 Express Bootloader** is similar for the **SAMD51J19**. These match the schematics above. 

Of course, the hex you're flashing doesn't have to be a bootloader... any program will compile into .hex or .bin. Forgive me for dawdling: 

CMSIS-DAP is just the tool that *connects* our computer to our microcontroller, we need also a program on our computer to operate the CMSIS-DAP. On this end, I've tested [pyOCD](https://github.com/pyocd/pyOCD) (for Python On Chip Debugger), but other tools exist. 

### 3.1: Install pyOCD

Their install documentation is better than mine, you can [read it here](https://github.com/pyocd/pyOCD#installing) to complete this step.

I (on windows) had to install python, visualstudio, and had to make sure [python was on my PATH](https://datatofish.com/add-python-to-windows-path/) so that I could invoke it from the command prompt. I also had to add the pyocd scripts to my PATH, they were located at `c\users\<username>\appdata\roaming\python39\scripts`. 

### 3.2: Setup pyOCD 

pyOCD comes out of the box with support for a bunch of microcontrollers, but not the ones I want (the D21 and D51 series). Target support is [managed well in pyOCD](https://github.com/pyocd/pyOCD/blob/master/docs/target_support.md) but I couldn't find my chips with their 'target find' tools, so I just downloaded them myself.

Downloads for Atmel Chips are at [https://packs.download.microchip.com/](https://packs.download.microchip.com/), you can `ctrl+f` for `D21` etc and get the most recent. **Importantly** you need to rename the `.atpack` to `.pack` - don't ask my why microchip is trying to use different file extensions than anyone else. 

To point pyOCD at these packs, they have a great little [yaml config](https://github.com/pyocd/pyOCD/blob/master/docs/configuration.md) tool. I made a new folder this exercise, and added a `pyocd.yaml` file there, like:

```yaml
pack:
  - Microchip.SAMD51_DFP.3.3.76.pack
  - Microchip.SAMD21_DFP.3.3.98.pack
```

That points pyOCD at these files (which are also in the folder) so that anytime I call a pyOCD script from this folder, it uses this config. Rad. I included that folder in this repo. 

Now I can do the magic: 
- connect the cmsis-dap tool to your circuit
- connect the cmsis-dap tool to your computer 
- power on my board (the cmsis-dap tool *measures* voltage, it doesn't provide it)
- open a terminal / command prompt / etc in the pyOCD folder with your config and hex file in it 
- run `pyocd flash -t <your target> <your .hex or .bin>`
- i.e. `pyocd flash -t atsamd51j19a bootloader-feather_m4-v3.10.0.bin`

To find your target name, you can do `pyocd list --targets`

## 4a: Programming, Debugging in ARM MDK with CMSIS-DAP

If you want to go beyond, and use these tools to program and debug chips, I have also spent *some* time trying to figure out how to set that up. 

To verify I could flash my board using the CMSIS-DAP tool, I first tried operating it with the [ARM MDK](http://www2.keil.com/mdk5) which is an install that will bring with it everything you need to write and flash code onto any ARM microcontroller. 

This can be a little intimidating if you've not experienced big IDEs before, but it's well supported and used commercially, so it's a good ground truth also. Here's what I did:

OK, this looks like it works, but it was a bit messy, here are some brief notes.
- install [the ARM MDK](https://www2.keil.com/mdk5) that includes 'uVision' (IDE)
- this manages device packs, a lot like pyOCD, but:
- had to install package manually, https://packs.download.microchip.com/ find D21 support package, download, rename .atpack to .pack, [import manually](https://www.keil.com/support/man/docs/uv4/uv4_ca_packinst_imp.htm) in keil package manager 
- setup a new project in uVision: the wizard is OK.
- add a new `main.c` file to the project, recall boilerplate c code:
    - `#include "samde18a.h" // include the hardware definitions`
    - `int main(void){<some test code>}`
- setup the project to use cmsis-dap debugger 
    - tools ... setup flash tools ... find tab 'debug'
    - 'use' cmsis-dap debugger
    - do 'setup' to the right 
    - do 'port' SW (not JTAG) 
- build your project with `F7`
- flash your board with `F8` 

Those are pretty rough notes, but I can testify that this works well on a windows machine.

## 4b: Programming, Debugging in Platformio with CMSIS-DAP 

[Platformio](https://platformio.org/) is a great tool that I've been using to write all of my embedded code lately. It's an extension for [VSCode](https://code.visualstudio.com/), which is essentially Microsoft's fork of [atom](https://atom.io/) (in case you're interested). 

Using CMSIS-DAP for a platformio project is [supported](https://docs.platformio.org/en/latest/plus/debug-tools/cmsis-dap.html), but I still need to figure out how to set it up for use on a custom board... this is on the way. 

## 4c: Programming with EDBG

[edbg](https://github.com/ataradov/edbg) is a simple command-line programming tool to flash binaries using a CMSIS-DAP programmer. 
