### Que?

If the above statement doesn't make immediate sense, here's what's up:

Every time our our microcontroller is reset, it starts operating the program that currently resides in its memory. This means that in order to load a new program onto the microcontroller, we need some way of writing into that memory from another device.

This normally means that we write (human readable) code on our PC / Laptop / Whatever, compile it using a *toolchain* that knows how our device is shaped (alignments, bus widths, processor architectures etc) into byte code (machine readable), and then use some device to write that byte-code into the microcontroller's memory. Then, next time it wakes up (or we reset it) the microcontroller starts running our program! 

Microcontrollers have pins that are specifically made for loading programs into their memory - these are known as 'debug ports' and ARM chips commonly use [JTAG](https://en.wikipedia.org/wiki/JTAG) or [SWD](https://en.wikipedia.org/wiki/JTAG#Similar_interface_standards). Our programming device (here it's the CMSIS-DAP) communicates to our PC using a USB connection on one side, and to our microcontroller using JTAG or SWD on the other side. 

Different microcontrollers have different debug ports: AVR chips have ISP (In System Programming) debug ports, XMEGA chips use PDI, and some ATTINYs use UPDI - these are [well explained here](https://www.kanda.com/blog/microcontrollers/avr-microcontrollers/avr-microcontroller-programming-interfaces-isp-jtag-tpi-pdi-updi/). CMSIS-DAP only talks to JTAG or SWD ports: others need other programmers, i.e. the [ATMEL-ICE](https://www.microchip.com/DevelopmentTools/ProductDetails/ATATMEL-ICE) [AVR ISP](https://www.microchip.com/DevelopmentTools/ProductDetails/PartNO/ATAVRISP2) or [FAB ISP](https://fab.cba.mit.edu/classes/863.16/doc/projects/ftsmin/index.html) that you can make yourself. 

Here's an abbreviated list of the system layers (not aligned) we're putting together when we do this:

**rows are not aligned**

| Human Codes | Compilers | Byte Code -> Device Port (tool) | Device Debug Ports |
| --- | --- | --- | --- |
| c | avr-gcc | cmsis-dap | jtag |
| c++ | gnu-arm-gcc | atmel-ice | swd |
| | | avr-isp | pdi |
| | | fab-isp | isp |
| | | usb | updi |
| | | | bootloader (self program) |

### What's a bootloader?

Occasionally, we don't need a programming device in order to load code onto microcontrollers, as is the case with Arduino and other development boards. What gives? 

These boards have [*bootloaders*](https://learn.adafruit.com/bootloader-basics) already written into their program memory. These are little programs that run *in the microcontroller* when they are reset. They start up, listen for some communication from your computer telling them that a new program is incoming, and if one doesn't exist, continue to the code loaded in the rest of their memory. 

Basically, when a bootloader is present, the microcontroller itself takes on the role of the 'byte code -> device port' tool: this is known as 'self-programming' - the microcontroller writes your new program into its own memory. 

Trouble is, when we make our own boards, we need to be able to load our own bootloaders (bootstrapping them...) and we need to do this with a tool like a CMSIS-DAP, AVR ISP etc. 